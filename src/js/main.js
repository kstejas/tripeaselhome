

$('#trip_carousel').carousel({
  pause: true,
interval: false
}).on('slide.bs.carousel', function (e) {

    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $('.carousel-item').length;
    
    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});


   $(document).ready(function(){

    $(".left").click(function(){

      $("#trip-destinations div").animate({ right: "0" });
        $(".right.carousel-control-next").css("visibility", "visible");
      $(".left.carousel-control-prev").css("visibility", "hidden");


    
    });
    $(".right").click(function(){

      $("#trip-destinations div").animate({ right: "+100%" });
      $(".left.carousel-control-prev").css("visibility", "visible");
      $(".right.carousel-control-next").css("visibility", "hidden");
    
    });
        $(".left1").click(function(){

      $("#trip-experiences div").animate({ right: "0" });
        $(".right1.carousel-control-next").css("visibility", "visible");
      $(".left1.carousel-control-prev").css("visibility", "hidden");


    
    });
    $(".right1").click(function(){

      $("#trip-experiences div").animate({ right: "+100%" });
      $(".left1.carousel-control-prev").css("visibility", "visible");
      $(".right1.carousel-control-next").css("visibility", "hidden");
    
    });

    if($(window).width() < 575){

$("#trip-destinations,#trip-bloggers").addClass("no-gutters");
}

$("#search-btn").click(function(){
   $("#search-bar").show();
 });
$("#search-bar-close").click(function(){
   $("#search-bar").hide();
 });

   

    $('.navbar-toggler').click(function(){
        if($(this).scrollTop() < 30){
          $("html, body").animate({ scrollTop: 31 }, 100);
      }
        
    });

     $('navbar').addClass("nav-inverse");
   

     var checkScrollBar = function(){

  if(($(this).scrollTop() < 42) && ($(this).scrollTop() > 30)){
    $('.navbar').toggle().fadeIn(1000);
 }

 if($(this).scrollTop() > 200){
    $("#search-bar").hide();
 }


        $("#navbarsExampleDefault ul li a").css({
         color: $(this).scrollTop() > 30 ?
           'rgb(0, 0, 0)' : 'rgb(255, 255, 255)'
       });
       $('.navbar-custom').css({
         backgroundColor: $(this).scrollTop() > 30 ?
           'rgb(255, 255, 255)' : 'transparent'
       });
       $('#logo').attr({
         "src": $(this).scrollTop() > 30 ?
           'assets/logo.png' : 'assets/logo-white.png'
       });
       $('#logo').addClass({
         "src": $(this).scrollTop() > 30 ?
           'assets/logo.png' : 'assets/logo-white.png'
       });
    
     }
     $(window).on('load resize scroll', checkScrollBar)
     });

// $(document).ready(function(){
//     $(document).scroll(function() {
//         var alpha = Math.min(0.5 + 0.4 * $(this).scrollTop() / 210, 0.9);
//         var channel = Math.round(alpha * 255);
//         $("body").css('background-color', 'rgb(' + channel + ',' + channel + ',' + channel + ')');
//     });
// });